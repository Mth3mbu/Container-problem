﻿using ContainerLoading1;
using NUnit.Framework;

namespace ContainerTestsV1
{
    [TestFixture]
    public class ContainerTests
    {
        [Test]
        public void Given_A_String_Of_A_Letter_Then_Return_Minimum_Number_Of_Stacks()
        {
            StackProcessor _stackProcessor = new StackProcessor();

            int results = _stackProcessor.GetMinimumNumberOfStacks("A");

            Assert.AreEqual(1, results);
        }

        [Test]
        public void Given_A_String_Set_Of_Letters_Then_Return_Minimum_Number_Of_Stacks()
        {
            StackProcessor _stackProcessor = new StackProcessor();

            int results = _stackProcessor.GetMinimumNumberOfStacks("CBACBACBACBACBA");

            Assert.AreEqual(3, results);
        }

        [Test]
        public void Given_A_String_Set_Of_Duplicated_Ascending_Letters_Then_Return_Minimum_Number_Of_Stacks()
        {
            StackProcessor _stackProcessor = new StackProcessor();

            int results = _stackProcessor.GetMinimumNumberOfStacks("CCCCBBBBAAAA");

            Assert.AreEqual(1, results);
        }

        [Test]
        public void Given_A_String_Set_Of_Duplicated_Mixed_Letters_Then_Return_Minimum_Number_Of_Stacks()
        {
            StackProcessor _stackProcessor = new StackProcessor();

            int results = _stackProcessor.GetMinimumNumberOfStacks("ACMICPC");

            Assert.AreEqual(4, results);
        }

        [Test]
        public void Given_A_String_Set_Of_Mixed_Up_Letters_Then_Return_Minimum_Number_Of_Stacks()
        {
            StackProcessor _stackProcessor = new StackProcessor();

            int results = _stackProcessor.GetMinimumNumberOfStacks("ZBCA");

            Assert.AreEqual(2, results);
        }
    }
}