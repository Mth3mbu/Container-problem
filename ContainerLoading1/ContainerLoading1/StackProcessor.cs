﻿using System.Collections;
using System.Collections.Generic;

namespace ContainerLoading1
{
    public class StackProcessor
    {
        public int GetMinimumNumberOfStacks(string alphabets)
        {
            List<Stack> stackList = new List<Stack>();

            stackList.Add(new Stack());
            stackList[0].Push(alphabets[0]);

            for (int i = 1; i < alphabets.Length; i++)
            {
                for (int x = 0; x < stackList.Count; x++)
                {
                    if ((alphabets[i].Equals((char)stackList[x].Peek()))
                        || ((char)stackList[x].Peek() > alphabets[i]))
                    {
                        stackList[x].Push(alphabets[i]);

                        break;
                    }

                    else if ((stackList.Count - 1 == x))
                    {
                        stackList.Add(new Stack());
                        stackList[stackList.Count - 1].Push(alphabets[i]);

                        break;
                    }
                }
            }

            return stackList.Count;
        }
    }
}
