﻿using System.Collections;
using System.Collections.Generic;

namespace Stack_Processor
{
    public class StackProcessor
    {
        public List<Stack> ProcessStacks(List<Stack> stackList, string containers)
        {
            for (int i = 1; i < containers.Length; i++)
            {
                for (int x = 0; x < stackList.Count; x++)
                {
                    if ((containers[i].Equals((char)stackList[x].Peek()))
                        || ((char)stackList[x].Peek() > containers[i]))
                    {
                        stackList[x].Push(containers[i]);

                        break;
                    }
                    else if ((stackList.Count - 1 == x))
                    {
                        stackList.Add(new Stack());
                        stackList[stackList.Count - 1].Push(containers[i]);

                        break;
                    }
                }
            }
            return stackList;
        }
    }
}
