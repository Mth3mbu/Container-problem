﻿using System;
using System.Collections;
using System.Collections.Generic;
using Packing_Service;

namespace ContainerV2
{
    public class Program
    {
        public static void Main(string[] args)
        {
            PackingService _packingService = new PackingService();

            string containers = _packingService.PromptUser();

            List<Stack> myStack = _packingService.PackContainers(containers);

            Console.Write("\n");

            _packingService.DisplayStackItems(myStack);

            Console.ReadKey(true);
        }
    }
}
