﻿using Stack_Processor;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Packing_Service
{
    public class PackingService
    {
        public List<Stack> PackContainers(string containers)
        {
            StackProcessor _stackProcesor = new StackProcessor();

            List<Stack> stackList = new List<Stack>();

            AddStack(stackList);
            stackList[0].Push(containers[0]);

            _stackProcesor.ProcessStacks(stackList, containers);

            return stackList;
        }

        public void AddStack(List<Stack> stacks)
        {
            stacks.Add(new Stack());
        }

        public string PromptUser()
        {
            Console.WriteLine("Enter a string of charecters");

            return Console.ReadLine().ToString();
        }

        public void DisplayStackItems(List<Stack> stacks)
        {
           int max = GetMaximumStacks(stacks);

            for (int i = 0; i < max; i++)
            {
                for (int x = 0; x < stacks.Count; x++)
                {
                    if (stacks[x].Count > 0)
                    {
                        Console.Write(stacks[x].Pop());
                    }

                    else
                    {
                        Console.Write(" ");
                    }
                }

                Console.Write("\n");
            }
        }

        public int GetMaximumStacks(List<Stack> stacks)
        {
            int max = 0;

            for (int x = 0; x < stacks.Count; x++)
            {
                if (stacks.Count > max)
                {
                    max = stacks.Count;
                }
            }

            return max;
        }
    }
}